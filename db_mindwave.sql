-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Nov 2019 pada 14.47
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mindwave`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(18) NOT NULL,
  `nama` varchar(80) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_admin`
--

INSERT INTO `tbl_admin` (`id_admin`, `username`, `password`, `nama`, `status`) VALUES
(1, 'grand48ola', '123', 'Hari Ilham Iswandi', 'Super Admin'),
(2, 'raihannisa', '123', 'Raihannisa HR', 'Admin'),
(3, 'fikri', '123', 'Fikri Firdaus', 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `jenis_kelamin` char(1) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `nilai_iq` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `nama`, `email`, `jenis_kelamin`, `tgl_lahir`, `nilai_iq`) VALUES
(1, 'Muhammad Hibatur Akmal', 'papapia@gmail.com', 'L', '1999-07-21', 106),
(2, 'Evan Ibrahim', 'mainichi@gmail.com', 'L', '1999-06-25', 125),
(3, 'Fuad Hasyim', 'fuadhasyim@gmail.com', 'L', '2000-10-26', 98),
(4, 'Hena', 'maulanaevan@gmail.com', 'L', '1998-10-19', 141),
(5, 'Farhan Firdaus', 'farfirus@gmail.com', 'L', '1999-10-29', 126),
(6, 'Yandra Permi Putri', 'yandrapermiputri@gmail.com', 'P', '1999-07-31', 130),
(7, 'Ilham Rahmad Setiadianto', 'ilhamrahmad@gmail.com', 'L', '1999-07-17', 121),
(8, 'Ikhsan Kamil', 'ikhsankamil@gmail.com', 'L', '1999-11-11', 106),
(9, 'Aoban Kaokab', 'aobankaokab_@gmail.com', 'L', '1998-12-31', 87),
(10, 'Rafid Budi', 'budiRafid@gmail.com', 'L', '1999-06-12', 129);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
