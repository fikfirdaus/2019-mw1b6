<?php
	session_start();
	if(!isset($_SESSION['username']))
		header("location:admin_login.php?pesan=invalid");
?>
<?php
	if(isset($_GET['pesan'])){
		if($_GET['pesan'] == "dibatasi"){
			echo "Hak akses dibatasi, anda bukan Super Admin !";
		}
	}	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Menu Admin | MindWave</title>
</head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script type="text/javascript" src="../js/script.js"></script>
<body>
	<div id="container">
		<section id="menu" class="sticky">
			<div class="kiri">
				<div id="logo">
					<a href="menu_admin.php"><img src="../img/logo.png"></a>
				</div>
			</div>
			<div class="tengah">
				<p>ID     : <?php echo $_SESSION["username"]; ?>
				</p>
			</div>
			<div class="kanan">
				<a href="logout_admin.php">Log-out</a>
			</div>
			<div class="clear"></div>
		</section>
		<section class="hitam-menu" id="intro-menu" style="padding-top: 130px; height: auto;" ><br>
			<div>
				<div class="tbl">
					<h2 style="text-align: center;">Admin Table</h2><br>
					<table width="80%" border="1">
					<tr style="background-color: transparent;"><th>ID Admin</th><th>Username</th><th>Nama</th><th>Status</th><th>Perintah</th></tr>
					<?php
					include "koneksi.php";

					$q = "SELECT * from tbl_admin";
					$h = mysqli_query($koneksi,$q);
					foreach ($h as $row){
					echo "<tr style='background-color:white; color:black;'>
							<td>".$row['id_admin']."</td>
							<td>".$row['username']."</td>
							<td>".$row['nama']."</td>
							<td>".$row['status']."</td>
							<td><a id='delete' onClick='return konfirmasi()' href='delete_admin.php?id_admin=".$row['id_admin']."'>Hapus</a></td>
						</tr>";
				
					}
					?>
				</table>
				<div style="text-align: center;">
				<a style="color: white;" href="edit_admin.php">Edit Data Saya</a>&nbsp; &nbsp;
				<a style="color: white;" href="tambah_admin.php">Tambah Admin</a>
				</div>
			</div><br>
				<a href="tampil_tbl_user.php">Tabel User</a><br><br><br>
				<a href="tampil_tbl_admin.php">Tabel Admin</a>
				<a href="menu_admin.php">Laman Utama</a>
			</div>
		</section>
		<section class="abu" id="copyright">
			<p>Copyright &copy; 2019 - Kelompok 6 (WEB IPB TEK 3B P1). All rights reserved</p>
		</section>
	</div>
</body>
</html>