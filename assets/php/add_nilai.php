<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>User | MindWave</title>
</head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script src="../js/fusioncharts.js"></script>
	<script src="../js/themes/fusioncharts.theme.candy.js"></script>
	<script src="../js/themes/fusioncharts.theme.carbon.js"></script>
	<script src="../js/themes/fusioncharts.theme.fint.js"></script>
	<script src="../js/themes/fusioncharts.theme.fusion.js"></script>
	<script src="../js/themes/fusioncharts.theme.gammel.js"></script>
	<script src="../js/themes/fusioncharts.theme.ocean.js"></script>
	<script src="../js/themes/fusioncharts.theme.zune.js"></script>
	<script src="../DataTables/jQuery-3.3.1/jquery-3.3.1.js"></script>
	<script type = "text/javascript" >
	history.pushState(null, null, '#');
	window.addEventListener('popstate', function(event)
	{
	history.pushState(null, null, '#');
	});
	</script>
	<script>
			FusionCharts.ready(
				function(){
					var theme = window.document.getElementById('theme');
					var type = window.document.getElementById('type');

					function renderChart(theme,type){
						var chart = new FusionCharts(
							 {
							 	type:type,
							 	dataFormat:'jsonurl',
							 	renderAt:'chart',
							 	dataSource:'dataRange.php?theme='+theme,
							 	events:{
							 		rendered: function(evt,arg){
							 			var chartRef = evt.sender;

							 			function updateData(){
							 				var max = 141;
							 				var min = 70;
							 				var val = Math.floor(Math.random()*(max-min+1)+min);
							 				strData = "&value="+val;
							 				chartRef.feedData(strData);
							 			}
							 			chartRef.intervalUpdateId = setInterval(updateData,100);
							 		},
							 	    disposed: function(evt,arg){
							 	    	clearInterval(evt.sender.intervalUpdateId);
							 	    }	
							 	}	
							 }	
							);

						chart.render();
					}

					theme.onchange =  function (){
						renderChart(theme.value, type.value);
						alert('Silahkan gunakan alat untuk memulai !!')
						//renderChart(theme.value, "bulb");
					}

					type.onchange =  function (){
						theme.onchange();
					}
					theme.onchange();
				}
			);	
	</script>
	<script>
			$(document).ready(function(){
				window.setInterval(function () {
					var sisawaktu = $("#waktu").html();
					sisawaktu = eval(sisawaktu);
					if (sisawaktu == 0) {
						location.href = "user_hitung.php";
					} else {
						$("#waktu").html(sisawaktu - 1);
					}
				}, 1000);
			});
		</script>
		<style type="text/css">
			#waktu {
				font-size:25pt;
				color:red;
			}
		</style>
<body>
	<div id="container">
		<section id="menu" class="sticky">
			<div class="kiri">
				<div id="logo">
					<img src="../img/logo.png">
				</div>
			</div>
			<div class="tengah">
				<p>
					Name : <?php echo $_SESSION["nama"]; ?><br>
					Tanggal Lahir : <?php echo $_SESSION["tgl_lahir"];?>
				</p>
			</div>
			<div class="clear"></div>
		</section>
		<section class="hitam-login" id="intro-login">
		<section class="grafik">
			<h2>MindWave</h2><br>
		<select id="theme" hidden="true">
			<option value="candy">Candy</option>
		</select>
		<select id="type" hidden="true">
			<option value="bulb">Bulb</option>
		</select>
		<form action="user_hitung.php" method="post">
		<div id="chart"></div>
		<h5>Generate Otomatis Dalam <span id="waktu">20</span> Sec</h5>
		<input hidden="true" type="text" name="nilai_iq" id="nilai_iq" value="<?php rand(70,141); ?>">
		</form>
		</section>
	</section>
		<section class="abu" id="copyright">
			<p>Copyright &copy; 2019 - Kelompok 6 (WEB IPB TEK 3B P1). All rights reserved</p>
		</section>
	</div>
</body>
</html>