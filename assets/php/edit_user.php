<?php
	session_start();
	if(!isset($_SESSION['username']))
		header("location:admin_login.php?pesan=invalid");

?>
<!DOCTYPE html>
<html>
<head>
	<title>Menu Admin | MindWave</title>
</head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script type="text/javascript" src="../js/script.js"></script>
<body>
	<div id="container">
		<section id="menu" class="sticky">
			<div class="kiri">
				<div id="logo">
					<a href="menu_admin.php"><img src="../img/logo.png"></a>
				</div>
			</div>
			<div class="tengah">
				<p>ID : <?php echo $_SESSION["username"]; ?>
				</p>
			</div>
			<div class="kanan">
				<a href="logout_admin.php">Log-out</a>
			</div>
			<div class="clear"></div>
		</section>
		<section class="hitam-menu" id="intro-menu" style="padding-top: 130px; height: auto;"><br>
			<div>
				<div class="edit">
			<?php
				$kiriman = $_GET["id_user"];
				include "koneksi.php";

				$q = "SELECT * FROM tbl_user WHERE id_user='$kiriman'";
				$result = mysqli_query($koneksi,$q);
				$row = mysqli_fetch_assoc($result);

			?>

				<section id="user_edit">
				<form action="proses_edit_user.php" method="post" onSubmit="return validasi()">
					<h2>Edit User</h2><br>
					<input type="text" name="nama" id="nama" placeholder="Nama Lengkap ..." value="<?php echo $row["nama"]; ?>"><br><br>
					<input type="text" name="email" id="email" placeholder="Email ..." value="<?php echo $row["email"]; ?>"><br><br>
					<input type="radio" name="jk" id="jk" value="L" checked="checked"> Laki - Laki&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="jk" id="jk" value="P"> Perempuan<br><br>
					<input type="date" name="tgl_lahir" id="tgl_lahir" value="<?php echo $row["tgl_lahir"]; ?>"><br>
					<input type="submit" name="masuk" value="Simpan" id="btn-masuk">
					<a href="tampil_tbl_user.php" id="btn-ragu">Kembali</a>
					<input type="hidden" name="id_user" value="<?php echo $row["id_user"]; ?>">
				</form>
			</section>
			</div>
		</section>
		<section class="abu" id="copyright">
			<p>Copyright &copy; 2019 - Kelompok 6 (WEB IPB TEK 3B P1). All rights reserved</p>
		</section>
	</div>
</body>
</html>