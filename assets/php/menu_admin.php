<?php
	session_start();
	if(!isset($_SESSION['username']))
		header("location:admin_login.php?pesan=invalid");

?>
<!DOCTYPE html>
<html>
<head>
	<title>Menu Admin | MindWave</title>
</head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script type="text/javascript" src="../js/script.js"></script>
<body>
	<div id="container">
		<section id="menu" class="sticky">
			<div class="kiri">
				<div id="logo">
					<a href="menu_admin.php"><img src="../img/logo.png"></a>
				</div>
			</div>
			<div class="tengah">
				<p>ID : <?php echo $_SESSION["username"]; ?>
				</p>
			</div>
			<div class="kanan">
				<a href="logout_admin.php">Log-out</a>
			</div>
			<div class="clear"></div>
		</section>
		<section class="hitam-menu" id="intro-menu" style="padding-top: 180px; height: 500px;"><br>
			<div>
				<h1><img src="../img/intro.png"><br><b>Mind<span>W</span>ave</b><br><br><i>"Selamat Datang, <span><?php echo $_SESSION["username"]; ?>
				</span>"</i></h1><br><br>
				<a href="tampil_tbl_user.php">Tabel User</a><br><br><br>
				<a href="tampil_tbl_admin.php">Tabel Admin</a>
				<a href="menu_admin.php">Laman Utama</a>
			</div>
		</section>
		<section class="abu" id="copyright">
			<p>Copyright &copy; 2019 - Kelompok 6 (WEB IPB TEK 3B P1). All rights reserved</p>
		</section>
	</div>
</body>
</html>