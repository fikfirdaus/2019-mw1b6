<?php
	session_start();
 		$chart = array(
 				'showValue'=>'1',
 				'theme'=>$_GET['theme'],
 				'lowerLimit'=>'0',
 				'upperLimit'=>'141',
 				'lowerLimitDisplay'=>'Min',
 				'upperLimitDisplay'=>'Max'	
 			);

 		$colorRange= array(
 				'color'=> array(
 						array('minValue'=>'0', 'maxValue'=>'29','code'=>'#FF0000'),
 						array('minValue'=>'30', 'maxValue'=>'49','code'=>'#ff3300'),
 						array('minValue'=>'50', 'maxValue'=>'69','code'=>'#ff6600'),
 						array('minValue'=>'70', 'maxValue'=>'79','code'=>'#ffff00'),
 						array('minValue'=>'80', 'maxValue'=>'89','code'=>'#ffff66'),
 						array('minValue'=>'90', 'maxValue'=>'109','code'=>'#99ff66'),
 						array('minValue'=>'110', 'maxValue'=>'119','code'=>'#66ff66'),
 						array('minValue'=>'120', 'maxValue'=>'129','code'=>'#33cc33'),
 						array('minValue'=>'130', 'maxValue'=>'139','code'=>'#009933'),
 						array('minValue'=>'140', 'maxValue'=>'200','code'=>'#0033cc'),
 					)
 			);

 		echo json_encode(array('chart'=>$chart,'colorRange'=>$colorRange,'value'=>$_SESSION["nilai_iq"]));

 ?>