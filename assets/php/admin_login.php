<!DOCTYPE html>
<html>
<head>
	<title>Admin Login | MindWave</title>
</head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script type="text/javascript" src="../js/script.js"></script>
<body>
	<?php
	if(isset($_GET['pesan'])){
		if($_GET['pesan'] == "gagal"){
			echo "Login gagal, username atau password salah !";
		}else if($_GET['pesan'] == "logout"){
			echo "Anda telah logout !";
		}elseif ($_GET['pesan'] == "invalid") {
			echo "Sesi anda habis !";
		}
	}	
	?>

	<div id="container">
		<section id="menu" class="sticky">
			<div class="kiri">
				<div id="logo">
					<a href="../../index.html"><img src="../img/logo.png"></a>
				</div>
			</div>
			<div class="tengah">
				<ul>
					<li><a href="../../index.html#intro">Intro</a></li>
					<li><a href="../../index.html#about_us">About Us</a></li>
					<li><a href="../../index.html#services">Services</a></li>
					<li><a href="../../index.html#contact">Contact</a></li>
				</ul>
			</div>
			<div class="clear"></div>
		</section>
		<section class="hitam-login" id="intro-login"><br>
			<section class="login_admin">
				<form action="login_admin.php" method="post">
					<h2>Login</h2><br>
					<input type="text" name="username" placeholder="Username"><br><br>
					<input type="password" name="password" placeholder="******"><br><br>
					<input type="submit" value="Masuk" id="btn-masuk">
					<a href="../../index.html" id="btn-kembali">Kembali</a>
				</form>
			</section>
		</section>
		<section class="abu" id="copyright">
			<p>Copyright &copy; 2019 - Kelompok 6 (WEB IPB TEK 3B P1). All rights reserved</p>
		</section>
	</div>
</body>
</html>