<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Hasil Nilai | MindWave</title>
</head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script src="../js/fusioncharts.js"></script>
	<script src="../js/themes/fusioncharts.theme.candy.js"></script>
	<script src="../js/themes/fusioncharts.theme.carbon.js"></script>
	<script src="../js/themes/fusioncharts.theme.fint.js"></script>
	<script src="../js/themes/fusioncharts.theme.fusion.js"></script>
	<script src="../js/themes/fusioncharts.theme.gammel.js"></script>
	<script src="../js/themes/fusioncharts.theme.ocean.js"></script>
	<script src="../js/themes/fusioncharts.theme.zune.js"></script>
	<script type = "text/javascript" >
	history.pushState(null, null, '#');
	window.addEventListener('popstate', function(event)
	{
	history.pushState(null, null, '#');
	});
	</script>
	<script>
			FusionCharts.ready(
				function(){
					var theme = window.document.getElementById('theme');
					var type = window.document.getElementById('type');

					function renderChart(theme,type){
						var chart = new FusionCharts(
							 {
							 	type:type,
							 	dataFormat:'jsonurl',
							 	renderAt:'chart',
							 	dataSource:'hasil_range.php?theme='+theme,
							 	events:{
							 		rendered: function(evt,arg){
							 			var chartRef = evt.sender;

							 			function updateData(){
							 				var max = 141;
							 				var min = 70;
							 				var val = $_SESSION["nilai_iq"];
							 				strData = "&value="+val;
							 				chartRef.feedData(strData);
							 			}
							 			chartRef.intervalUpdateId = setInterval(updateData,100);
							 		},
							 	    disposed: function(evt,arg){
							 	    	clearInterval(evt.sender.intervalUpdateId);
							 	    }	
							 	}	
							 }	
							);

						chart.render();
					}

					theme.onchange =  function (){
						renderChart(theme.value, type.value);
						//alert('Silahkan gunakan alat untuk memulai !!')
						//renderChart(theme.value, "bulb");
					}

					type.onchange =  function (){
						theme.onchange();
					}
					theme.onchange();
				}
			);	
	</script>
<body>
	<div id="container">
		<section id="menu" class="sticky">
			<div class="kiri">
				<div id="logo">
					<img src="../img/logo.png"></a>
				</div>
			</div>
			<div class="tengah">

			</div>
			<div class="clear"></div>
		</section>
		<section class="hitam-login" id="intro-login" style="padding-bottom: 280px;"><br>
			<section class="hasil_akhir">
				<h2 style="text-align: center;">HASIL IQ</h2><br>
				<pre>Nama		: <?php echo $_SESSION["nama"]; ?></pre>
				<pre>Email		: <?php echo $_SESSION["email"]; ?></pre>
				<pre>Jenis Kelamin 	: <?php $_SESSION["jk"]; if($_SESSION["jk"]=='L') {echo "Laki - Laki";} else {echo "Perempuan";} ?></pre>
				<pre>Tanggal Lahir	: <?php echo $_SESSION["tgl_lahir"]; ?></pre>
				<pre>Score		: <?php echo $_SESSION["nilai_iq"]; ?></pre>
				<pre>Tergolong	: <?php $_SESSION["nilai_iq"]; 
					if($_SESSION["nilai_iq"] >0 && $_SESSION["nilai_iq"]<30) {echo "Idiot";}
					else if($_SESSION["nilai_iq"] >29 && $_SESSION["nilai_iq"]<50) {echo "Imbecile";}
					else if($_SESSION["nilai_iq"] >50 && $_SESSION["nilai_iq"]<70) {echo "Moron or Debil";} 
					else if($_SESSION["nilai_iq"] >69 && $_SESSION["nilai_iq"]<80) {echo "Bordeline";}
					else if($_SESSION["nilai_iq"] >79 && $_SESSION["nilai_iq"]<90) {echo "Below Average";}
					else if($_SESSION["nilai_iq"] >89 && $_SESSION["nilai_iq"]<110) {echo "Normal";}
					else if($_SESSION["nilai_iq"] >109 && $_SESSION["nilai_iq"]<120) {echo "Above Average";}
					else if($_SESSION["nilai_iq"] >119 && $_SESSION["nilai_iq"]<130) {echo "Superior";}
					else if($_SESSION["nilai_iq"] >129) {echo "Very Superior or Genius";}
					else{echo "Error";} ?><br>
					<div id="chart" style="text-align: center;"></div>
				</pre>

		<select id="theme" hidden="true">
			<option value="carbon">carbon</option>
		</select>
		<select id="type" hidden="true">
			<option value="bulb">Bulb</option>
		</select>
			<div style="text-align: center;">
			<a href="user_sesi.php">Laman Utama</a>
			<a href="simpan_file.php">Save File</a>
			</div>
		</section>
		</section>
		<section class="abu" id="copyright">
			<p>Copyright &copy; 2019 - Kelompok 6 (WEB IPB TEK 3B P1). All rights reserved</p>
		</section>
	</div>
</body>
</html>