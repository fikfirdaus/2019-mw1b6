<?php
	session_start();
	if(!isset($_SESSION['username']))
		header("location:admin_login.php?pesan=invalid");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Menu Admin | MindWave</title>
</head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script type="text/javascript" src="../js/script.js"></script>
	<link rel="stylesheet" type="text/css" href="../DataTables/Bootstrap-4-4.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../DataTables/DataTables-1.10.20/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="../style.css">
	<link rel="stylesheet" href="../DataTables/DataTables-1.10.20/css/jquery.dataTables.css">
	<link rel="stylesheet" href="../DataTables/Buttons-1.6.1/css/buttons.dataTables.css">

	<script src="../DataTables/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
	<script src="../DataTables/Bootstrap-4-4.1.1/js/bootstrap.min.js"></script>
	<script src="../DataTables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<script src="../DataTables/DataTables-1.10.20/js/jquery.dataTables.min.js"></script>
	<script src="../DataTables/jQuery-3.3.1/jquery-3.3.1.js"></script>
	<script src="../DataTables/DataTables-1.10.20/js/jquery.dataTables.js"></script>
	<script src="../DataTables/Buttons-1.6.1/js/dataTables.buttons.js"></script>
	<script src="../DataTables/JSZip-2.5.0/jszip.js"></script>
	<script src="../DataTables/pdfmake-0.1.36/pdfmake.js"></script>
	<script src="../DataTables/pdfmake-0.1.36/vfs_fonts.js"></script>
	<script src="../DataTables/Buttons-1.6.1/js/buttons.html5.js"></script>

	<script>
		$('document').ready(function () {
			$('#tbb').DataTable({
				paging: true,
				searching: true,
				ordering: true,
				info: true,
				dom: 'Bfrtip',
				buttons: [
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            {
		            	extend: 'pdfHtml5',
		            	text: 'PDF',
		            	orientation: 'landscape',
		            	pageSize: 'legal',
		            	customize: function (doc) {
		            		doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
		            	},
		            	title: function () {
		            		return 'MindWave User Table';
		            	}
		        	}
		        ]
			});
		});
	</script>

<body>
	<div id="container">
		<section id="menu" class="sticky" style="position: absolute;">
			<div class="kiri">
				<div id="logo">
					<a href="menu_admin.php"><img src="../img/logo.png"></a>
				</div>
			</div>
			<div class="tengah">
				<p>ID : <?php echo $_SESSION["username"]; ?>
				</p>
			</div>
			<div class="kanan">
				<a href="logout_admin.php">Log-out</a>
			</div>
			<div class="clear"></div>
		</section>
		<section class="hitam-menu" id="intro-menu" style="padding-top: 30px; height: auto;"><br><br>
		<section style="text-align: center;">
			<h2 style="color: white;">User Table</h2>
<table style="background-color: transparent;">
	<td>
	<table id="tbb" border="2">
		<tbody>

	<?php
		include "koneksi.php";

		$result = mysqli_query($koneksi, 'SELECT * FROM tbl_user');

		while ($row = mysqli_fetch_assoc($result)) {
	?>

		<tr>
			<td align="left"><?php echo $row["id_user"];?></td>
	        <td align="left"><?php echo $row["nama"];?></td>
	        <td align="left"><?php echo $row["email"];?></td>
	        <td align="left"><?php echo $row["jenis_kelamin"];?></td>
	        <td align="left"><?php echo $row["tgl_lahir"];?></td>
	        <td align="left"><?php echo $row["nilai_iq"];?></td>
	        <td align="left"><a style="border-radius: 0;padding: 0; background-color: transparent; color: black;" href="delete_user.php?id_user=<?php echo $row['id_user']; ?>" id="delete" onclick="return konfirmasi()">Hapus</a> | 
	        	<a style="border-radius: 0;padding: 0; background-color: transparent; color: black;" href="edit_user.php?id_user=<?php echo $row['id_user']; ?>" id="edit">Edit</a>
		</tr>

	<?php } ?>

	</tbody>
	<thead style="color: white;">
		<tr>
			<th>ID User</th>
			<th>Nama</th>
			<th>Email</th>
			<th>Jenis Kelamin</th>
			<th>Tanggal Lahir</th>
			<th>Nilai IQ</th>
			<th>Perintah</th>
		</tr>
	</thead>
	</table>
	</td>
</table>
</section>
				<a href="tampil_tbl_user.php">Tabel User</a><br><br><br>
				<a href="tampil_tbl_admin.php">Tabel Admin</a>
				<a href="menu_admin.php">Laman Utama</a>
			</div>
		</section>
		<section class="abu" id="copyright">
			<p>Copyright &copy; 2019 - Kelompok 6 (WEB IPB TEK 3B P1). All rights reserved</p>
		</section>
	</div>
</body>
</html>