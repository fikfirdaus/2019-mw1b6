<?php
	session_start();
	if(!isset($_SESSION['username']))
		header("location:admin_login.php?pesan=invalid");

?>

<?php

	if(isset($_GET['pesan'])){
		if($_GET['pesan']=="gagal"){
			echo "Password tidak sesuai, silahkan coba lagi !";
		}
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Menu Admin | MindWave</title>
</head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script type="text/javascript" src="../js/script.js"></script>
<body>
	<div id="container">
		<section id="menu" class="sticky">
			<div class="kiri">
				<div id="logo">
					<a href="menu_admin.php"><img src="../img/logo.png"></a>
				</div>
			</div>
			<div class="tengah">
				<p>ID : <?php echo $_SESSION["username"]; ?>
				</p>
			</div>
			<div class="kanan">
				<a href="logout_admin.php">Log-out</a>
			</div>
			<div class="clear"></div>
		</section>
		<section class="hitam-menu" id="intro-menu" style="padding-top: 130px; height: auto;"><br>
			<div>
				<div class="edit">
			<?php
				$kiriman = $_SESSION['username'];
				include "koneksi.php";

				$q = "SELECT * FROM tbl_admin WHERE username='$kiriman'";
				$result = mysqli_query($koneksi,$q);
				$row = mysqli_fetch_assoc($result);

			?>

				<section id="user_edit">
				<form action="proses_edit_admin.php" method="post" onSubmit="return validasi3()">
					<h2>Data Diri Admin</h2><br>
					<input type="hidden" name="username" id="username" placeholder="" value="<?php echo $row["username"]; ?>">
					<input type="text" name="nama" id="nama" placeholder="Nama" value="<?php echo $row["nama"]; ?>"><br><br>
					<input type="password" name="k_password" id="k_password" placeholder="Old Password ..."> 
					<input type="password" name="password" id="password" placeholder="New Password ..."><br>
					<input type="hidden" name="id_admin" id="id_admin" value="<?php echo $row["id_admin"]; ?>">
					<input type="submit" name="masuk" value="Simpan" id="btn-masuk">
					<a href="tampil_tbl_admin.php" id="btn-ragu">Kembali</a>
					<input type="hidden" name="id_user" value="<?php echo $row["id_user"]; ?>">
				</form>
			</section>
			</div>
		</section>
		<section class="abu" id="copyright">
			<p>Copyright &copy; 2019 - Kelompok 6 (WEB IPB TEK 3B P1). All rights reserved</p>
		</section>
	</div>
</body>
</html>