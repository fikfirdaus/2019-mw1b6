<?php
	session_start();
	if(!isset($_SESSION['username']))
		header("location:admin_login.php?pesan=invalid");

?>
<!DOCTYPE html>
<html>
<head>
	<title>Menu Admin | MindWave</title>
</head>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<script type="text/javascript" src="../js/script.js"></script>
<body>
	<div id="container">
		<section id="menu" class="sticky">
			<div class="kiri">
				<div id="logo">
					<a href="menu_admin.php"><img src="../img/logo.png"></a>
				</div>
			</div>
			<div class="tengah">
				<p>ID : <?php echo $_SESSION["username"]; ?>
				</p>
			</div>
			<div class="kanan">
				<a href="logout_admin.php">Log-out</a>
			</div>
			<div class="clear"></div>
		</section>
		<section class="hitam-menu" id="intro-menu" style="padding-top: 130px; height: auto;"><br>
			<div>
				<div class="edit">
				<section id="user_edit">
				<form action="proses_tambah_admin.php" method="post" onSubmit="return validasi2()">
					<h2>Tambah Admin</h2><br>
					<input type="text" name="username" id="username" placeholder="Username ..."><br><br>
					<input type="text" name="nama" id="nama" placeholder="Nama"><br><br>
					<input type="password" name="password" id="password" placeholder="New Password ..."><br><br>
					<input type="hidden" name="id_admin" id="id_admin">
					<input type="submit" name="masuk" value="Simpan" id="btn-masuk">
					<a href="tampil_tbl_admin.php" id="btn-ragu">Kembali</a>
				</form>
			</section>
			</div>
		</section>
		<section class="abu" id="copyright">
			<p>Copyright &copy; 2019 - Kelompok 6 (WEB IPB TEK 3B P1). All rights reserved</p>
		</section>
	</div>
</body>
</html>